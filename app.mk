
SOURCEDIR := .

APPS_ROOT_DIR := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))


APP_NAME := $(APPNAME)

APPSOURCEDIR := $(SOURCEDIR)/source

DISTREL := $(APPS_ROOT_DIR)/dist
ZIPREL := $(DISTREL)

# APP_ZIP_FILE := $(APPNAME).zip
APP_ZIP_FILE := $(ZIPREL)/$(APPNAME).zip


.PHONY: $(APPNAME)
$(APPNAME): manifest
	@echo "*** Creating $(APPNAME).zip ***"

	@echo "  >> removing old application zip $(APP_ZIP_FILE)"
	@if [ -e "$(APP_ZIP_FILE)" ]; then \
		rm -f $(APP_ZIP_FILE); \
	fi

	@echo "  >> creating destination directory $(ZIPREL)"
	@if [ ! -d $(ZIPREL) ]; then \
		mkdir -p $(ZIPREL); \
	fi

	@echo "  >> setting directory permissions for $(ZIPREL)"
	@if [ ! -w $(ZIPREL) ]; then \
		chmod 755 $(ZIPREL); \
	fi

	@echo "  >> creating application zip $(APP_ZIP_FILE)"
	@if [ -d $(SOURCEDIR) ]; then \
		(zip -0 -r "$(APP_ZIP_FILE)" . -i \*.png $(ZIP_EXCLUDE)); \
		(zip -9 -r "$(APP_ZIP_FILE)" . -x \*~ -x \*.png -x Makefile $(ZIP_EXCLUDE)); \
	else \
		echo "Source for $(APPNAME) not found at $(SOURCEDIR)"; \
	fi

	@if [ "$(IMPORTCLEANUP)" ]; then \
		echo "  >> deleting imports";\
		rm -r -f $(APPSOURCEDIR)/common; \
	fi \

	@echo "*** packaging $(APPNAME) complete ***"
