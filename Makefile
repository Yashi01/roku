

APPNAME = allritesSDK
VERSION = 1.0

ZIP_EXCLUDE= -x \*.pkg -x storeassets\* -x app.mk -x package\* -x Procfile\* -x dist\* -x node\* -x .git\* -x .DS_Store\* -x keys\* -x \*/.\*

include ./app.mk