## allrites roku SDK documentation
----

### Interface of the allrites analytic SDK task node
#### FIELDS
- **error** : type="assocarray"
*errors will be logged despite dubug field flag*

- **debug** : type="bool", default value = false
*If debug flag is set to true allrites events will be printed to debug console*


- **setup** : type="assocarray"
*setup field is for setting configs and thus triggering listening events an object/assocarray with attributes. e.g.*
    >{ 
    >    '*MANDATORY attrs*
    >    player   : as roSGNode ' a reference to a client's player
    >    apiToken : as roString
    >    clientId : as roInt
    >
    >    '*OPTIONAL attrs*
    >    interval : as roInt    ' between 10 and 30 sec
    >    debug    : as roBoolean
    >}


- **VideoId** : type="integer"
*a setting videoId will trigger a getting and setting VideoDetails from the server*


- **activityStatus** : type="assocarray"
*an object/assocarry containing the client's verification details, e.g.:*
    >{ 
    >    error : as roBoolean 
    >    data  : as roAssociativeArray 
    >}
if client's verification status is failed the analytic won't work


#### METHODS
- **getConfigs** : type="function", returns roAssociativeArray
*getConfig returns an object with allrites current configs if any*

----

### Initialization steps

1. Create node ***ComponentLibrary*** in a roku scene that has a video player to send analytics and append it to current scene
```brightscript
sub init()
    '...
    m.allritesSDK = CreateObject( "roSGNode", "ComponentLibrary" )
    m.allritesSDK.id = "allritesSDK"
    m.allritesSDK.uri = "https://allrites-roku-sdk.herokuapp.com/allritesSDK.zip"
    m.top.appendChild( m.allritesSDK )
```

2. Add a listener for ***loadStatus*** field to further initialization when loading will have the *'ready'* status
``` brightscript
'... in init
    m.allritesSDK.observeField("loadStatus", "onLoadStatusChanged")
end sub
```

3. At a *'ready'* event of loadStatus we registered previously, create a task node from ***"allritesComponentLib:allrites"*** 
``` brightscript

sub onLoadStatusChanged()
    if m.allritesSDK.loadStatus = "ready"
        m.allritesTask = CreateObject( "roSGNode", "allritesComponentLib:allrites" )
    '...
```

4. create config object for analytic task component for setup and initialize it
``` brightscript
    '... in onLoadStatusChanged
        m.Config = {
            'mandatory
            player   : m.player,
            apiToken : m.global.allritesConfigs.apiToken,
            clientId : m.global.allritesConfigs.clientId,
        }
        m.allritesTask.setup = m.Config
    endif
end sub
```

----
##### All initialization code
``` brightscript
sub init()
    '...
    m.allritesSDK = CreateObject( "roSGNode", "ComponentLibrary" )
    m.allritesSDK.id = "allritesSDK"
    m.allritesSDK.uri = "https://allrites-roku-sdk.herokuapp.com/allritesSDK.zip"
    m.top.appendChild( m.allritesSDK )
    m.allritesSDK.observeField("loadStatus", "onLoadStatusChanged")
    '...
end sub

sub onLoadStatusChanged()
    if m.allritesSDK.loadStatus = "ready"
        m.allritesTask = CreateObject( "roSGNode", "allritesComponentLib:allrites" )

        m.Config = {
            'mandatory
            player   : m.player,
            apiToken : m.global.allritesConfigs.apiToken,
            clientId : m.global.allritesConfigs.clientId,

            'optional
            interval : 30, 
            debug    : true 
        }
        m.allritesTask.setup = m.Config
        m.allritesTask.observeField("error", "allritesOnError")
    endif
end sub

'on Errors listener
sub allritesOnError( msg )
    errorData = msg.getData()
    print errorData.msg
end sub
```
----
