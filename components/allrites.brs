sub init()

#CONST DEV = true

    m.baseURL       = "https://content.allrites.com"
    m.baseAPIURL    = "https://api.allrites.com"
    m.device        = "roku"
    m.top.activityStatus = { error : false }

    m.port = createObject( "roMessagePort" )
    m.top.observeField( "setup", m.port )
    m.top.observeField( "videoId", m.port )
    m.top.observeField( "start", m.port )
    m.top.observeField( "error", "onError" )

    m.top.functionName = "listen"

    m.APIToken      = ""
    m.clientID      = 0
    m.interval      = 30
    
    m.videoData     = invalid
    m.timer         = invalid
    m.player        = invalid
    m.view_id       = invalid
    m.beginning     = true

    m.jobsById      = {}

    m.top.control   = "run"
end sub

function listen()
    allritesPrintMsg( "initialization" )

    TRY
    while true
        msg = wait( 0, m.port )

        mt = type( msg )
        if mt = "roSGNodeEvent"

            if msg.getField() = "setup"
                configs = checkValOrTHROW( msg.getData(), "roAssociativeArray", "Configs" )

                ' check player
                m.player = checkValOrTHROW( configs.player, "roSGNode", "Player" )
                if not m.player.hasField("position") or not m.player.hasField("state")
                    ' m.top.error = {
                    '     msg : "player should be in setup field and should have fields such as position and state"
                    ' }
                    THROW "Invalid Player has not Position or State field"
                    ' exit while
                end if
                m.apiToken = checkValOrTHROW( configs.apiToken,   "roString", "APIToken" )
                m.clientId = checkValOrTHROW( configs.clientId,   "roInt", "ClientID" )
                
                m.top.debug= checkValOrDefault( configs.debug,    "roBoolean", false )
                m.interval = checkValOrDefault( configs.interval, "roInt", 30 )
                if not (m.interval >= 10 and m.interval <= 30) then m.interval = 30

                act = makeApiActivityPostReqSync()
                if act <> 200 then
                    m.top.activityStatus = {
                        error  : true,
                        msg : "client's ID or Token is invalid"
                    }
                    THROW "client's ID or Token is invalid"
                end if

                createTimer()
                'observing player events
                m.player.observeField( "position", m.port )
                m.player.observeField( "state", m.port )

                if configs.videoId <> invalid
                    m.videoId = configs.videoId
                    makeVideoDetailsGetReq()
                end if
                
            else if msg.getField() = "videoId"
                data = msg.getData()
                if data <> invalid and type( data ) = "Integer"
                    m.videoId = data
                    makeVideoDetailsGetReq()
                end if

            else if msg.getField() = "start"
                data = msg.getData()
                if data.videoId <> invalid 

                    m.videoId = data.videoId
                    makeVideoDetailsGetReq()

                    m.beginning = true

                end if

            else if msg.getField() = "state"

                state = msg.getData()

                allritesPrintMsg( "player's state", state )

                if state = "playing" then 
                    'beginning
                    timestamp = invalid
                    if m.player.position < 10 and m.beginning then 
                        'creating timestamp as milliseconds
                        timestamp = CreateObject("roLongInteger") 
                        time = CreateObject("roDateTime")
                        ts = time.AsSeconds()
                        tms = time.GetMilliseconds()
                        timestamp.SetLongInt( ts )
                        timestamp.SetLongInt( timestamp.GetLongInt() * 1000 )
                        timestamp.SetLongInt( timestamp.GetLongInt() + tms )
                        'viewEnd
                        viewEnd = 0 

                        if m.timer = invalid then createTimer()
                        if m.view_id = invalid then m.view_id = vuid()

                        if m.videoId = invalid and m.top.VideoDetails = invalid
                            url = m.player.content.url
                            'regex for extracting video ID from url
                            'find the video ID with slashes
                            rVidID = CreateObject("roRegex", "\/([0-9]{1,6})\/", "g" )
                            vid = rVidID.Match( url )
                            'now remove slashes
                            rSlash = CreateObject("roRegex", "\/", "g" )
                            videoId = rSlash.replaceAll( vid[0], "")

                            if videoId <> invalid and type( videoId ) = "String"
                                m.videoId = val( videoId, 10 )
                                videoDetails = ParseJson( makeVideoDetailsGetReqSync() ).data
                                setVideoDetails( videoDetails )
                                m.view_id = vuid()
                            else
                                m.top.error = { msg : "can't define video ID" }
                            end if
                            rVidID = invalid
                            rSlash = invalid 
                        end if

                    else 
                        viewEnd = Cint( m.player.position * 1000 )
                    end if

                    m.timer.control = "start"

                    if timestamp = invalid then 
                        makeAnalyticPOSTReq({ view_start : 0, view_end : viewEnd })
                    else
                        makeAnalyticPOSTReq({ view_start : 0, view_end : viewEnd }, timestamp.GetLongInt() )
                    end if
                    m.beginning = false
                end if

                if state = "paused" then 
                    makeAnalyticPOSTReq({ view_start : 0, view_end : Cint( m.player.position * 1000) })
                    m.timer.control = "stop"
                end if

                if state = "finished" then 
                    makeAnalyticPOSTReq({ view_start : 0, view_end : Cint( m.player.position * 1000) })
                    m.timer.control = "stop"
                    m.beginning = true
                    m.videoId = invalid
                    m.view_id = invalid
                end if

                if state = "stopped" then 
                    makeAnalyticPOSTReq({ view_start : 0, view_end : Cint( m.player.position * 1000) })
                    m.timer.control = "stop"
                    m.beginning = true
                    m.videoId = invalid
                    m.view_id = invalid
                end if

            else if msg.getField() = "fire" and m.player.state = "playing"
                makeAnalyticPOSTReq({ view_start : 0, view_end : Cint( m.player.position * 1000) })
            end if

        else if mt = "roUrlEvent"

            processResponse( msg )

            if m.player <> invalid and ( m.player.state = "stopped" or m.player.state = "finished" )
                clearState()
            end if
        end if

    end while

    CATCH ERR
        m.top.error = {
            msg : ERR.message
        }
    END TRY
        
    allritesPrintMsg( "stopped", "unknown error" )
    clearState()

    return true

end function

sub makeAnalyticPostReq( viewTime = { view_start : 0, view_end : 0 }, timestamp = invalid ) 

    if m.top.VideoDetails <> invalid and m.top.VideoDetails.video_id <> invalid
        uri = m.BaseAPIUrl + "/views/v1/video"

        urlTransfer = CreateObject("roUrlTransfer")
        urlTransfer.SetCertificatesFile("common:/certs/ca-bundle.crt")
        urlTransfer.AddHeader("X-Roku-Reserved-Dev-Id", "")
        urlTransfer.InitClientCertificates()
        urlTransfer.AddHeader("Content-Type","application/json; charset=utf-8" )
        urlTransfer.AddHeader("Authorization", m.apiToken )

        urlTransfer.SetRequest("POST")

        urlTransfer.setPort( m.port )
        urlTransfer.setUrl( uri )

        data = m.top.VideoDetails

        data.id = m.view_id
        data.view_start = viewTime.view_start
        data.view_end = viewTime.view_end
        data.timestamp = timestamp

        d = FormatJson( data )

        if urlTransfer.AsyncPostFromString( d )
            id = stri( urlTransfer.getIdentity() ).trim()
            m.jobsById[ id ] = { xfer: urlTransfer, reqName : "analyticReq" }
        end if
    else 
        m.top.error = {
            msg : "no video details data"
        }
        m.timer.control = "stop"
    end if

end sub

function makeApiActivityPostReqSync() 
    uri = m.baseUrl + "/api/api-activity"

    urlTransfer = CreateObject("roUrlTransfer")
    urlTransfer.SetCertificatesFile("common:/certs/ca-bundle.crt")
    urlTransfer.AddHeader("X-Roku-Reserved-Dev-Id", "")
    urlTransfer.InitClientCertificates()
    urlTransfer.AddHeader("Content-Type","application/x-www-form-urlencoded" )
    urlTransfer.SetRequest("POST")
    
    urlTransfer.setPort( m.port )
    urlTransfer.setUrl( uri )

    d = "token=" + m.apiToken + "&company_id=" + stri( m.clientId ).trim() + "&device=roku"

    return urlTransfer.PostFromString( d )

end function

sub makeVideoDetailsGetReq() 
    uri = m.baseUrl + "/api/video"

    urlTransfer = CreateObject("roUrlTransfer")
    urlTransfer.SetCertificatesFile("common:/certs/ca-bundle.crt")
    urlTransfer.AddHeader("X-Roku-Reserved-Dev-Id", "")
    urlTransfer.InitClientCertificates()
    urlTransfer.AddHeader("Content-Type","application/x-www-form-urlencoded" )
    urlTransfer.SetRequest("GET")
    urlTransfer.setPort( m.port )

    d = "token=" + m.apiToken + "&video_id=" + stri( m.videoId ).trim()
    uri = uri + "?" + d

    urlTransfer.setUrl( uri )

    if urlTransfer.AsyncGetToString()
        id = stri( urlTransfer.getIdentity() ).trim()
        m.jobsById[ id ] = { xfer: urlTransfer, reqName : "videoDataReq" }
    end if
end sub

function makeVideoDetailsGetReqSync() 
    uri = m.baseUrl + "/api/video"

    urlTransfer = CreateObject("roUrlTransfer")
    urlTransfer.SetCertificatesFile("common:/certs/ca-bundle.crt")
    urlTransfer.AddHeader("X-Roku-Reserved-Dev-Id", "")
    urlTransfer.InitClientCertificates()
    urlTransfer.AddHeader("Content-Type","application/x-www-form-urlencoded" )
    urlTransfer.SetRequest("GET")

    d = "token=" + m.apiToken + "&video_id=" + stri( m.videoId ).trim()
    uri = uri + "?" + d

    urlTransfer.setUrl( uri )

    return urlTransfer.GetToString()

end function

function processResponse( msg = invalid )
    TRY
        if msg <> invalid
            data = msg.getString()
            code = msg.getResponseCode()
            
            idKey = stri(msg.GetSourceIdentity()).trim()
            job = m.jobsById[ idKey ]

            'API Success
            if code = 200
                json = ParseJson( msg.getString() )

                'save video data for analytic
                if job.reqName = "videoDataReq" and json.data <> invalid then 
                    setVideoDetails( json.data )
                end if
                'activityStatus 
                if job.reqName = "activityReq" and json <> invalid then 
                    m.top.activityStatus = {
                        error : false
                        data  : json
                    }
                end if

                allritesPrintMsg( "api response", job.reqName )


            'API Error
            else 
                errMsg = msg.GetFailureReason()
                m.top.error = {
                    msg : errMsg
                    code : code
                }
                'activityStatus 
                if job.reqName = "activityReq" and code < 0 then 
                    m.top.activityStatus = {
                        error  : true,
                        msg : "client's ID or Token is invalid"
                    }
                    m.top.error = { msg : "client's ID or Token is invalid" }
                end if
                ' for brake the loop
                return false

            end if

            'delete saved urlTransfer job
            m.jobsById.delete( idKey )
        end if
    CATCH err 
        m.top.error = {
            msg : err.message
        }
    END TRY
    return true
end function

function vuid()
    rndm = (100000000 + Rnd(0) * 900000000 )
    return Cint( rndm )
end function

function checkValOrDefault( variable, typ as string, default )
    if variable = invalid or type( variable ) <> typ then return variable = default
    return variable
end function

function checkValOrTHROW( variable, typ as string, fieldName )
    if variable = invalid or type( variable ) <> typ then THROW "invalid field " + fieldName
    return variable
end function

sub setVideoDetails( videoDetail as Object )
    

    if videoDetail.duration <> invalid then duration = val(videoDetail.duration, 10) * 60000 else duration = 0
    if videoDetail.video_episode <> invalid then video_episode = videoDetail.video_episode else video_episode = ""

    m.top.VideoDetails = {
        video_id        : videoDetail.id,
        title           : videoDetail.title,
        duration        : duration, 
        video_series    : videoDetail.isSeries,
        video_episode   : video_episode,
        company_id      : m.clientId,
        device          : m.device,
        token           : m.apiToken, 
        ip_address      : invalid
    }

end sub

sub onError( msg )
    allritesPrintMsg( "error", msg.getData().msg )
end sub

sub allritesPrintMsg( status = "activity", msg = "", data = ""  )
    if m.top.debug or status = "error"
        print "[ allrites: " + status + " ]: " + msg, data
    end if
end sub

sub createTimer()
    m.timer = CreateObject("roSGNode", "Timer")
    m.timer.observeField("fire", m.port )
    m.timer.duration = m.interval
    m.timer.repeat = true
end sub

sub clearState()
    ' if m.player <> invalid
    '     m.player.unObserveField( "position" )
    '     m.player.unObserveField( "state" )
    ' end if
    m.videoData = invalid
    m.timer = invalid
    
    ' m.player = invalid
    m.view_id = invalid
    m.jobsById = {}
end sub

function getConfigs()
    return {
        baseURL    : m.baseURL,
        baseAPIURL : m.baseAPIURL,
        APIoken    : m.APIToken,
        clientID   : m.clientID,
        interval   : m.interval,
        debug      : m.top.debug
    }
end function
